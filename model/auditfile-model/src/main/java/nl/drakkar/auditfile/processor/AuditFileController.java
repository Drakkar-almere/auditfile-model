package nl.drakkar.auditfile.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import nl.auditfiles.xaf._3.Auditfile;
import nl.auditfiles.xaf._3.*;

public class AuditFileController {

	
	public static void processXml(BufferedReader reader) throws IOException, JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(Auditfile.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		Auditfile auditFile = (Auditfile) jaxbUnmarshaller.unmarshal(reader);
		
		System.out.println("Processing :" + auditFile.getCompany().getCompanyName());
		
		List<CustomerSupplier> customerSuppliers = auditFile.getCompany().getCustomersSuppliers().getCustomerSupplier();
		
		CustomerSupplierProcessor.processCustomers(customerSuppliers);


	}
}
