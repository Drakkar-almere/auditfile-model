package nl.drakkar.auditfile.processor;
import nl.auditfiles.xaf._3.CustomerSupplier;
import java.util.List;

public class CustomerSupplierProcessor {
	
	public static void processCustomers(List<CustomerSupplier> customerSuppliers) {
		
		for(CustomerSupplier customer: customerSuppliers) {	
			
			switch (customer.getCustSupTp().name()) {
				case "C" : processCustomer(customer); break;
				case "S" : processSupplier(customer); break;
			}
		}
	}
	/**
	 * Process the CustomerSupplier as Customer (CustSubId = "C")
	 * 
	 * @param customer
	 */
	public static void processCustomer(CustomerSupplier customer) {
		System.out.println("CUSTOMER = " + customer.getCustSupName());
	}
	
	/**
	 * Process the CustomerSupplier as Customer (CustSubId = "S")
	 * 
	 * @param supplier
	 */
	public static void processSupplier(CustomerSupplier supplier) {
		System.out.println("SUPPLIER = " + supplier.getCustSupName());
	}

}
