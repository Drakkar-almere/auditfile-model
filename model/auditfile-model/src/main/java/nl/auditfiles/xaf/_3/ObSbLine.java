//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.18 at 01:03:52 PM CET 
//


package nl.auditfiles.xaf._3;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nr" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35"/>
 *         &lt;element name="obLineNr" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35"/>
 *         &lt;element name="desc" type="{http://www.auditfiles.nl/XAF/3.2}String9999" minOccurs="0"/>
 *         &lt;element name="amnt" type="{http://www.auditfiles.nl/XAF/3.2}Amount2decimals"/>
 *         &lt;element name="amntTp" type="{http://www.auditfiles.nl/XAF/3.2}Debitcredittype"/>
 *         &lt;element name="docRef" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *         &lt;element name="recRef" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *         &lt;element name="matchKeyID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="custSupID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="invRef" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *         &lt;element name="invPurSalTp" type="{http://www.auditfiles.nl/XAF/3.2}Purchasesalestype" minOccurs="0"/>
 *         &lt;element name="invTp" type="{http://www.auditfiles.nl/XAF/3.2}Debitcredittype" minOccurs="0"/>
 *         &lt;element name="invDt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="invDueDt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="mutTp" type="{http://www.auditfiles.nl/XAF/3.2}Mutationtype" minOccurs="0"/>
 *         &lt;element name="costID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="prodID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="projID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="artGrpID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="qntityID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *         &lt;element name="qntity" type="{http://www.auditfiles.nl/XAF/3.2}Nonnegativeinteger10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nr",
    "obLineNr",
    "desc",
    "amnt",
    "amntTp",
    "docRef",
    "recRef",
    "matchKeyID",
    "custSupID",
    "invRef",
    "invPurSalTp",
    "invTp",
    "invDt",
    "invDueDt",
    "mutTp",
    "costID",
    "prodID",
    "projID",
    "artGrpID",
    "qntityID",
    "qntity"
})
public class ObSbLine {

    @XmlElement(required = true)
    protected String nr;
    @XmlElement(required = true)
    protected String obLineNr;
    protected String desc;
    @XmlElement(required = true)
    protected BigDecimal amnt;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected Debitcredittype amntTp;
    protected String docRef;
    protected String recRef;
    protected String matchKeyID;
    protected String custSupID;
    protected String invRef;
    @XmlSchemaType(name = "string")
    protected Purchasesalestype invPurSalTp;
    @XmlSchemaType(name = "string")
    protected Debitcredittype invTp;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar invDt;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar invDueDt;
    @XmlSchemaType(name = "string")
    protected Mutationtype mutTp;
    protected String costID;
    protected String prodID;
    protected String projID;
    protected String artGrpID;
    protected String qntityID;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger qntity;

    /**
     * Gets the value of the nr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNr() {
        return nr;
    }

    /**
     * Sets the value of the nr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNr(String value) {
        this.nr = value;
    }

    /**
     * Gets the value of the obLineNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObLineNr() {
        return obLineNr;
    }

    /**
     * Sets the value of the obLineNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObLineNr(String value) {
        this.obLineNr = value;
    }

    /**
     * Gets the value of the desc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Sets the value of the desc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesc(String value) {
        this.desc = value;
    }

    /**
     * Gets the value of the amnt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmnt() {
        return amnt;
    }

    /**
     * Sets the value of the amnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmnt(BigDecimal value) {
        this.amnt = value;
    }

    /**
     * Gets the value of the amntTp property.
     * 
     * @return
     *     possible object is
     *     {@link Debitcredittype }
     *     
     */
    public Debitcredittype getAmntTp() {
        return amntTp;
    }

    /**
     * Sets the value of the amntTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Debitcredittype }
     *     
     */
    public void setAmntTp(Debitcredittype value) {
        this.amntTp = value;
    }

    /**
     * Gets the value of the docRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocRef() {
        return docRef;
    }

    /**
     * Sets the value of the docRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocRef(String value) {
        this.docRef = value;
    }

    /**
     * Gets the value of the recRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecRef() {
        return recRef;
    }

    /**
     * Sets the value of the recRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecRef(String value) {
        this.recRef = value;
    }

    /**
     * Gets the value of the matchKeyID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchKeyID() {
        return matchKeyID;
    }

    /**
     * Sets the value of the matchKeyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchKeyID(String value) {
        this.matchKeyID = value;
    }

    /**
     * Gets the value of the custSupID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustSupID() {
        return custSupID;
    }

    /**
     * Sets the value of the custSupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustSupID(String value) {
        this.custSupID = value;
    }

    /**
     * Gets the value of the invRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvRef() {
        return invRef;
    }

    /**
     * Sets the value of the invRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvRef(String value) {
        this.invRef = value;
    }

    /**
     * Gets the value of the invPurSalTp property.
     * 
     * @return
     *     possible object is
     *     {@link Purchasesalestype }
     *     
     */
    public Purchasesalestype getInvPurSalTp() {
        return invPurSalTp;
    }

    /**
     * Sets the value of the invPurSalTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Purchasesalestype }
     *     
     */
    public void setInvPurSalTp(Purchasesalestype value) {
        this.invPurSalTp = value;
    }

    /**
     * Gets the value of the invTp property.
     * 
     * @return
     *     possible object is
     *     {@link Debitcredittype }
     *     
     */
    public Debitcredittype getInvTp() {
        return invTp;
    }

    /**
     * Sets the value of the invTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Debitcredittype }
     *     
     */
    public void setInvTp(Debitcredittype value) {
        this.invTp = value;
    }

    /**
     * Gets the value of the invDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvDt() {
        return invDt;
    }

    /**
     * Sets the value of the invDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvDt(XMLGregorianCalendar value) {
        this.invDt = value;
    }

    /**
     * Gets the value of the invDueDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInvDueDt() {
        return invDueDt;
    }

    /**
     * Sets the value of the invDueDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInvDueDt(XMLGregorianCalendar value) {
        this.invDueDt = value;
    }

    /**
     * Gets the value of the mutTp property.
     * 
     * @return
     *     possible object is
     *     {@link Mutationtype }
     *     
     */
    public Mutationtype getMutTp() {
        return mutTp;
    }

    /**
     * Sets the value of the mutTp property.
     * 
     * @param value
     *     allowed object is
     *     {@link Mutationtype }
     *     
     */
    public void setMutTp(Mutationtype value) {
        this.mutTp = value;
    }

    /**
     * Gets the value of the costID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostID() {
        return costID;
    }

    /**
     * Sets the value of the costID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostID(String value) {
        this.costID = value;
    }

    /**
     * Gets the value of the prodID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdID() {
        return prodID;
    }

    /**
     * Sets the value of the prodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdID(String value) {
        this.prodID = value;
    }

    /**
     * Gets the value of the projID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjID() {
        return projID;
    }

    /**
     * Sets the value of the projID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjID(String value) {
        this.projID = value;
    }

    /**
     * Gets the value of the artGrpID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArtGrpID() {
        return artGrpID;
    }

    /**
     * Sets the value of the artGrpID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArtGrpID(String value) {
        this.artGrpID = value;
    }

    /**
     * Gets the value of the qntityID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQntityID() {
        return qntityID;
    }

    /**
     * Sets the value of the qntityID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQntityID(String value) {
        this.qntityID = value;
    }

    /**
     * Gets the value of the qntity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQntity() {
        return qntity;
    }

    /**
     * Sets the value of the qntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQntity(BigInteger value) {
        this.qntity = value;
    }

}
