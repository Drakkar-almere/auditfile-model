//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.18 at 01:03:52 PM CET 
//


package nl.auditfiles.xaf._3;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vatCode" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="vatID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35"/>
 *                   &lt;element name="vatDesc" type="{http://www.auditfiles.nl/XAF/3.2}String999"/>
 *                   &lt;element name="vatToPayAccID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *                   &lt;element name="vatToClaimAccID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vatCode"
})
public class VatCodes {

    protected List<VatCode> vatCode;

    /**
     * Gets the value of the vatCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vatCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatCode }
     * 
     * 
     */
    public List<VatCode> getVatCode() {
        if (vatCode == null) {
            vatCode = new ArrayList<VatCode>();
        }
        return this.vatCode;
    }

}
