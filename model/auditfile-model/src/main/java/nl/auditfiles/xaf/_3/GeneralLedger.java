//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.18 at 01:03:52 PM CET 
//


package nl.auditfiles.xaf._3;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GeneralLedger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GeneralLedger">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ledgerAccount" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="accID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35"/>
 *                   &lt;element name="accDesc" type="{http://www.auditfiles.nl/XAF/3.2}String999"/>
 *                   &lt;element name="accTp" type="{http://www.auditfiles.nl/XAF/3.2}Accounttype"/>
 *                   &lt;element name="leadCode" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                   &lt;element name="leadDescription" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                   &lt;element name="leadReference" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                   &lt;element name="leadCrossRef" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                   &lt;element name="taxonomy" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="taxoRef" type="{http://www.auditfiles.nl/XAF/3.2}String9999"/>
 *                             &lt;element name="entryPoint" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="entryPointRef" type="{http://www.auditfiles.nl/XAF/3.2}String9999"/>
 *                                       &lt;element name="conceptRef" type="{http://www.auditfiles.nl/XAF/3.2}String9999"/>
 *                                       &lt;element name="domainMember" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="domainMemberRef" type="{http://www.auditfiles.nl/XAF/3.2}String9999"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="changeInfo" type="{http://www.auditfiles.nl/XAF/3.2}ChangeInfo" minOccurs="0"/>
 *                   &lt;element name="glAccountHistory" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="glAccount" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="accID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35" minOccurs="0"/>
 *                                       &lt;element name="accDesc" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                                       &lt;element name="accTp" type="{http://www.auditfiles.nl/XAF/3.2}Accounttype" minOccurs="0"/>
 *                                       &lt;element name="leadCode" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                                       &lt;element name="leadDescription" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                                       &lt;element name="leadReference" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                                       &lt;element name="leadCrossRef" type="{http://www.auditfiles.nl/XAF/3.2}String999" minOccurs="0"/>
 *                                       &lt;element name="changeInfo" type="{http://www.auditfiles.nl/XAF/3.2}ChangeInfo" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="basics" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="basic" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="basicType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;length value="2"/>
 *                                   &lt;enumeration value="02"/>
 *                                   &lt;enumeration value="03"/>
 *                                   &lt;enumeration value="04"/>
 *                                   &lt;enumeration value="05"/>
 *                                   &lt;enumeration value="12"/>
 *                                   &lt;enumeration value="14"/>
 *                                   &lt;enumeration value="23"/>
 *                                   &lt;enumeration value="29"/>
 *                                   &lt;enumeration value="30"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="basicID" type="{http://www.auditfiles.nl/XAF/3.2}IdentificationString35"/>
 *                             &lt;element name="basicDesc" type="{http://www.auditfiles.nl/XAF/3.2}String9999"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GeneralLedger", propOrder = {
    "ledgerAccount",
    "basics"
})
public class GeneralLedger {

    @XmlElement(required = true)
    protected List<LedgerAccount> ledgerAccount;
    protected Basics basics;

    /**
     * Gets the value of the ledgerAccount property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ledgerAccount property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLedgerAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LedgerAccount }
     * 
     * 
     */
    public List<LedgerAccount> getLedgerAccount() {
        if (ledgerAccount == null) {
            ledgerAccount = new ArrayList<LedgerAccount>();
        }
        return this.ledgerAccount;
    }

    /**
     * Gets the value of the basics property.
     * 
     * @return
     *     possible object is
     *     {@link Basics }
     *     
     */
    public Basics getBasics() {
        return basics;
    }

    /**
     * Sets the value of the basics property.
     * 
     * @param value
     *     allowed object is
     *     {@link Basics }
     *     
     */
    public void setBasics(Basics value) {
        this.basics = value;
    }

}
