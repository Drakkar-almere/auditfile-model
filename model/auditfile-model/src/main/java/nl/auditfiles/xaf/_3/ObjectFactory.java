//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.03.18 at 01:03:52 PM CET 
//


package nl.auditfiles.xaf._3;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.auditfiles.xaf._3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.auditfiles.xaf._3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Auditfile }
     * 
     */
    public Auditfile createAuditfile() {
        return new Auditfile();
    }

    /**
     * Create an instance of {@link Header }
     * 
     */
    public Header createHeader() {
        return new Header();
    }

    /**
     * Create an instance of {@link Company }
     * 
     */
    public Company createCompany() {
        return new Company();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Vat }
     * 
     */
    public Vat createVat() {
        return new Vat();
    }

    /**
     * Create an instance of {@link GeneralLedger }
     * 
     */
    public GeneralLedger createGeneralLedger() {
        return new GeneralLedger();
    }

    /**
     * Create an instance of {@link Currency }
     * 
     */
    public Currency createCurrency() {
        return new Currency();
    }

    /**
     * Create an instance of {@link ChangeInfo }
     * 
     */
    public ChangeInfo createChangeInfo() {
        return new ChangeInfo();
    }

    /**
     * Create an instance of {@link CustomerSupplier }
     * 
     */
    public CustomerSupplier createCustomerSupplier() {
        return new CustomerSupplier();
    }

    /**
     * Create an instance of {@link BankAccount }
     * 
     */
    public BankAccount createBankAccount() {
        return new BankAccount();
    }

    /**
     * Create an instance of {@link CustomerSupplierHistory }
     * 
     */
    public CustomerSupplierHistory createCustomerSupplierHistory() {
        return new CustomerSupplierHistory();
    }

    /**
     * Create an instance of {@link LedgerAccount }
     * 
     */
    public LedgerAccount createLedgerAccount() {
        return new LedgerAccount();
    }

    /**
     * Create an instance of {@link Basics }
     * 
     */
    public Basics createBasics() {
        return new Basics();
    }

    /**
     * Create an instance of {@link Basic }
     * 
     */
    public Basic createBasic() {
        return new Basic();
    }

    /**
     * Create an instance of {@link Taxonomy }
     * 
     */
    public Taxonomy createTaxonomy() {
        return new Taxonomy();
    }

    /**
     * Create an instance of {@link GlAccountHistory }
     * 
     */
    public GlAccountHistory createGlAccountHistory() {
        return new GlAccountHistory();
    }

    /**
     * Create an instance of {@link GlAccount }
     * 
     */
    public GlAccount createGlAccount() {
        return new GlAccount();
    }

    /**
     * Create an instance of {@link EntryPoint }
     * 
     */
    public EntryPoint createEntryPoint() {
        return new EntryPoint();
    }

    /**
     * Create an instance of {@link DomainMember }
     * 
     */
    public DomainMember createDomainMember() {
        return new DomainMember();
    }

    /**
     * Create an instance of {@link CustomersSuppliers }
     * 
     */
    public CustomersSuppliers createCustomersSuppliers() {
        return new CustomersSuppliers();
    }

    /**
     * Create an instance of {@link VatCodes }
     * 
     */
    public VatCodes createVatCodes() {
        return new VatCodes();
    }

    /**
     * Create an instance of {@link Periods }
     * 
     */
    public Periods createPeriods() {
        return new Periods();
    }

    /**
     * Create an instance of {@link OpeningBalance }
     * 
     */
    public OpeningBalance createOpeningBalance() {
        return new OpeningBalance();
    }

    /**
     * Create an instance of {@link Transactions }
     * 
     */
    public Transactions createTransactions() {
        return new Transactions();
    }

    /**
     * Create an instance of {@link Journal }
     * 
     */
    public Journal createJournal() {
        return new Journal();
    }

    /**
     * Create an instance of {@link Subledgers }
     * 
     */
    public Subledgers createSubledgers() {
        return new Subledgers();
    }

    /**
     * Create an instance of {@link Subledger }
     * 
     */
    public Subledger createSubledger() {
        return new Subledger();
    }

    /**
     * Create an instance of {@link SbLine }
     * 
     */
    public SbLine createSbLine() {
        return new SbLine();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link TrLine }
     * 
     */
    public TrLine createTrLine() {
        return new TrLine();
    }

    /**
     * Create an instance of {@link ObLine }
     * 
     */
    public ObLine createObLine() {
        return new ObLine();
    }

    /**
     * Create an instance of {@link ObSubledgers }
     * 
     */
    public ObSubledgers createObSubledgers() {
        return new ObSubledgers();
    }

    /**
     * Create an instance of {@link ObSubledger }
     * 
     */
    public ObSubledger createObSubledger() {
        return new ObSubledger();
    }

    /**
     * Create an instance of {@link ObSbLine }
     * 
     */
    public ObSbLine createObSbLine() {
        return new ObSbLine();
    }

    /**
     * Create an instance of {@link Period }
     * 
     */
    public Period createPeriod() {
        return new Period();
    }

    /**
     * Create an instance of {@link VatCode }
     * 
     */
    public VatCode createVatCode() {
        return new VatCode();
    }

}
