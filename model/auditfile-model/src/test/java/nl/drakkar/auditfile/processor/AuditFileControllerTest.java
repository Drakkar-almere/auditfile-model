package nl.drakkar.auditfile.processor;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.bind.JAXBException;


public class AuditFileControllerTest {

	@Test
	public void itShouldReadXmlFile() throws IOException, JAXBException {

        ClassLoader classLoader = AuditFileControllerTest.class.getClassLoader();
        InputStream res = classLoader.getResourceAsStream("XmlAuditfileFinancieel3.2_test.xml");

		BufferedReader reader = new BufferedReader(new InputStreamReader(res));
		AuditFileController.processXml (reader);
	}
}